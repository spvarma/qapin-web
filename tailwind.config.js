const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./public/**/*.html', './src/**/*.vue'],

  darkMode: false,

  theme: {
    extend: {
      keyframes: {},

      colors: {
        primary: {
          500: '#f2613c',
        },
        gray: {
          100: '#e9ecef',
          200: '#eee',
        },
        white: '#FFFFF',
      },
    },
  },

  variants: {
    extend: {},
  },
  plugins: [],
}
