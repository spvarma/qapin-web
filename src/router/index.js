import Vue from 'vue'
import VueRouter from 'vue-router'

// Middleware route

// Auth Login & Registration and Forgot password

import Login from '../components/Login.vue'
import ForgotPassword from '../views/ForgotPassword.vue'
import BasicInfo from '../views/sign-up/BasicInfo'
import AboutProfile from '../views/sign-up/AboutProfile'
import ResetLink from '../views/ResetLink.vue'

// View Pages

import Dashboard from '../views/Dashboard.vue'
import NewsFeed from '../views/news-feed/NewsFeed.vue'
import FindJob from '../views/find-job/Index.vue'
import PostJob from '../views/post-job/Index.vue'
import ReviewPost from '../views/review-post/Index.vue'
import UserProfile from '../views/UserProfile.vue'
import Freelancer from '../views/freelancer/Index.vue'
import allContracts from '../views/freelancer/allContracts.vue'
import endContract from '../views/freelancer/endContract.vue'
import jobDetails from '../views/freelancer/jobDetails.vue'
import submitProposal from '../views/freelancer/submitProposal.vue'
import myProposals from '../views/freelancer/myProposals.vue'
import Offer from '../views/freelancer/Offer.vue'
import Milestones from '../views/freelancer/Milestones.vue'
import Transaction from '../views/freelancer/Transaction.vue'
import Profile from '../views/freelancer/Profile.vue'
import milestoneSubmition from '../views/freelancer/milestoneSubmition.vue'
import messagesFiles from '../views/freelancer/messagesFiles.vue'


Vue.use(VueRouter)



const routes = [


  // Auth Guard
  {
    path: '/dashboard',
    component: Dashboard,
    meta: { requiresAuth: true },
  },

  // default route

  {
    path: '/',
    name: 'Login',
    component: Login,
  },

  // Login
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },

  // Forgot Password
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    component: ForgotPassword,
    // meta: { requiresAuth: true },
  },

  // Reset Link
  {
    path: '/reset-link',
    name: 'ResetLink',
    component: ResetLink,
    // meta: { requiresAuth: true },
  },

  // Sign up
  {
    path: '/sign-up',
    name: 'BasicInfo',
    component: BasicInfo,
  },

  {
    path: '/sign-up/about-profile',
    name: 'AboutProfile',
    component: AboutProfile,
  },

  // View Pages routes

  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: { requiresAuth: true },
  },
  {
    path: '/news-feed',
    name: 'NewsFeed',
    component: NewsFeed,
    meta: { requiresAuth: true },
  },

  {
    path: '/find-job',
    name: 'FindJob',
    component: FindJob,
    meta: { requiresAuth: true },
  },

  {
    path: '/post-job',
    name: 'PostJob',
    component: PostJob,
    meta: { requiresAuth: true },
  },

  {
    path: '/review-post',
    name: 'ReviewPost',
    component: ReviewPost,
    meta: { requiresAuth: true },
  },

  {
    path: '/user-profile',
    name: 'UserProfile',
    component: UserProfile,
    meta: { requiresAuth: true },
  },

  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
  },
  
  
  {
    path: "/freelancer",
    name: "Freelancer",
    meta: { requiresAuth: true },
    component: Freelancer,
  },
  {
    path: "/freelancer/job-details",
    name: "jobDetails",
    meta: { requiresAuth: true },
    component: jobDetails,
  },
  {
    path: "/freelancer/end-contract",
    name: "endContract",
    meta: { requiresAuth: true },
    component: endContract,
  },
  {
    path: "/freelancer/submit-proposal",
    name: "submitProposal",
    meta: { requiresAuth: true },
    component: submitProposal,
  },
  {
    path: "/freelancer/all-contracts",
    name: "allContracts",
    meta: { requiresAuth: true },
    component: allContracts,
  },
  {
    path: "/freelancer/my-proposals",
    name: "myProposals",
    meta: { requiresAuth: true },
    component: myProposals,
  },
  {
    path: "/freelancer/offer",
    name: "Offer",          
    meta: { requiresAuth: true },
    component: Offer,
  },
  {
    path: "/freelancer/milestones",
    name: "Milestones",
    meta: { requiresAuth: true },
    component: Milestones,
  },
  {
    path: "/freelancer/transaction",
    name: "Transaction",
    meta: { requiresAuth: true },
    component: Transaction,
  },
  {
    path: "/freelancer/profile",
    name: "Profile",
    meta: { requiresAuth: true },
    component: Profile,
  },
  {
    path: "/freelancer/milestone-submition",
    name: "milestoneSubmition",
    meta: { requiresAuth: true },
    component: milestoneSubmition,
  },
  {
    path: "/freelancer/messages-files",
    name: "messagesFiles",
    meta: { requiresAuth: true },
    component: messagesFiles,
  },
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth)
  const isUserLogin = localStorage.getItem('token')

  if (requiresAuth && !isUserLogin) {
    next('/login')
  } else if (requiresAuth && isUserLogin) {
    next()
  } else {
    next()
  }
})

export default router
