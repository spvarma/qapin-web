import Vue from 'vue'
import Vuex from 'vuex'
import post from '../store/post'
import global from '../store/global'

// stores modules
Vue.use(Vuex)

export default new Vuex.Store({
  state: {},

  mutations: {},

  actions: {},

  modules: {
    post,
    global,
  },
})
