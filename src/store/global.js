import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const global = {
  namespaced: true,

  state: () => ({
    refreshNewsFeedData: Function,
  }),

  getters: {
    refreshNewsFeed: (state) => state.refreshNewsFeedData,
  },

  mutations: {
    REFRESH_NEWS_FEED(state, fun) {
      console.log(fun)
      state.refreshNewsFeedData = fun
    },
  },

  actions: {
    refreshNewsFeedFunc({ commit }, { refresh }) {
      console.log(refresh)
      commit('REFRESH_NEWS_FEED', refresh)
    },
  },
}
export default global
